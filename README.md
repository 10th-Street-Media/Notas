# Notas

Notas is a software platform where people can review whatever - books, movies, restaurants, concerts, or even a little piece of belly button fluff. However, site admins can create limits if they want, because maybe their website only does books reviews, restaurant reviews, or whatever. Each review on a subject contributes to its reputation score.
